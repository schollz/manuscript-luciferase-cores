import numpy as np
import random
import json
import operator

from tqdm import tqdm


m = np.load('markovMatrix.npy')

# pathways = []
# for ifoo in tqdm(range(1000)):
#     state = 0
#     pathway = []
#     while state != 13:
#         rand = random.random()
#         prevVal = 0
#         curVal = 0
#         for i,val in enumerate(m[state,:]):
#             curVal += val
#             if rand >= prevVal and rand < curVal:
#                 state = i
#                 pathway.append(state)
#                 break
#             prevVal = curVal
#     pathways += list(set(pathway))[1:-1]
# print(pathways)

pathways = {'N': 0, 'M': 0, 'C': 0, '?': 0}
pathwayNum = {}
for i in range(14):
    pathwayNum[str(i)] = 0
for ifoo in tqdm(range(1000)):
    isType = '?'
    state = 0
    pathway = []
    while state != 13:
        rand = random.random()
        prevVal = 0
        curVal = 0
        if state == 1 or state == 12:  # restart pathway
            isType = 'M'
            pathway = [x for x in pathway if x != 3]
            pathway = [x for x in pathway if x != 6]
            pathway = [x for x in pathway if x != 11]
        if state == 3 or state == 6:  # restart pathway
            isType = 'N'
            pathway = [x for x in pathway if x != 1]
            pathway = [x for x in pathway if x != 12]
            pathway = [x for x in pathway if x != 11]
        if state == 11:  # restart pathway
            isType = 'C'
            pathway = [x for x in pathway if x != 1]
            pathway = [x for x in pathway if x != 12]
            pathway = [x for x in pathway if x != 3]
            pathway = [x for x in pathway if x != 6]
        if state == 0:
            pathway = []
        for i, val in enumerate(m[state, :]):
            curVal += val
            if rand >= prevVal and rand < curVal:
                state = i
                break
            pathway.append(state)
            prevVal = curVal
    pathways[isType] += 1
    for i in list(set(pathway)):
        pathwayNum[str(i)] += 1

print(pathways)
sorted_pathways = sorted(
    pathwayNum.items(), key=operator.itemgetter(1), reverse=True)
print(sorted_pathways)

for i in pathwayNum:
    print(i, pathwayNum[i] * 100 / 10000)
# for i,val in enumerate(sorted_pathways):
#     if i > 10:
#         break
#     print(val)
#
# counts = {'1-':0,'3-':0,'11':0,'6-':0}
# for key in pathways.keys():
#     for key_count in counts:
#         if key_count == key[0:2]:
#             counts[key_count] += pathways[key]
# print(counts)


# MISFOLDED

misfoldedState = 10
pathways = {'N': 0, 'M': 0, 'C': 0, '?': 0,
            'Ntotal': 0, 'Mtotal': 0, 'Ctotal': 0, '?total': 0}
pathwayNum = {}
for i in range(14):
    pathwayNum[str(i)] = 0
for ifoo in tqdm(range(10000)):
    isType = '?'
    state = 0
    pathway = []
    while state != 13 and state != 4 and state != 7 and state != 10:
        rand = random.random()
        prevVal = 0
        curVal = 0
        if state == 3 or state == 6:
            isType = 'N'
        if state == 1 or state == 12:
            isType = 'M'
        if state == 11:
            isType = 'C'
        if state == 8:
            isType = '?'
        if state == 0:
            pathway = []
        for i, val in enumerate(m[state, :]):
            curVal += val
            if rand >= prevVal and rand < curVal:
                state = i
                break
            pathway.append(state)
            prevVal = curVal
    pathways[isType + 'total'] += 1
    if state != 13:
        pathways[isType] += 1
        for i in list(set(pathway)):
            pathwayNum[str(i)] += 1

# print(pathways)
# print(pathwayNum)
# sorted_pathways = sorted(pathwayNum.items(), key=operator.itemgetter(1), reverse=True)
# print(sorted_pathways)
print(misfoldedState, "MISFOLDED STATE PERCENTAGES")
for i in ['N', 'M', 'C']:
    print(i, 100 * pathways[i] / pathways[i + 'total'])


misfoldedState = 10
pathways = {'N': 0, 'M': 0, 'C': 0, '?': 0,
            'Ntotal': 0, 'Mtotal': 0, 'Ctotal': 0, '?total': 0}
pathwayNum = {}
for i in range(14):
    pathwayNum[str(i)] = 0
for ifoo in tqdm(range(10000)):
    isType = '?'
    state = 0
    pathway = []
    while state != 13 and state != misfoldedState:
        rand = random.random()
        prevVal = 0
        curVal = 0
        if state == 3 or state == 6:
            isType = 'N'
        if state == 1 or state == 12:
            isType = 'M'
        if state == 11:
            isType = 'C'
        if state == 8:
            isType = '?'
        if state == 0:
            pathway = []
        for i, val in enumerate(m[state, :]):
            curVal += val
            if rand >= prevVal and rand < curVal:
                state = i
                break
            pathway.append(state)
            prevVal = curVal
    pathways[isType + 'total'] += 1
    if state != 13:
        pathways[isType] += 1
        for i in list(set(pathway)):
            pathwayNum[str(i)] += 1

# print(pathways)
# print(pathwayNum)
# sorted_pathways = sorted(pathwayNum.items(), key=operator.itemgetter(1), reverse=True)
# print(sorted_pathways)
print(misfoldedState, "MISFOLDED STATE PERCENTAGES")
for i in ['N', 'M', 'C']:
    print(i, 100 * pathways[i] / pathways[i + 'total'])


misfoldedState = 4
pathways = {'N': 0, 'M': 0, 'C': 0, '?': 0,
            'Ntotal': 0, 'Mtotal': 0, 'Ctotal': 0, '?total': 0}
pathwayNum = {}
for i in range(14):
    pathwayNum[str(i)] = 0
for ifoo in tqdm(range(10000)):
    isType = '?'
    state = 0
    pathway = []
    while state != 13 and state != misfoldedState:
        rand = random.random()
        prevVal = 0
        curVal = 0
        if state == 3 or state == 6:
            isType = 'N'
        if state == 1 or state == 12:
            isType = 'M'
        if state == 11:
            isType = 'C'
        if state == 8:
            isType = '?'
        if state == 0:
            pathway = []
        for i, val in enumerate(m[state, :]):
            curVal += val
            if rand >= prevVal and rand < curVal:
                state = i
                break
            pathway.append(state)
            prevVal = curVal
    pathways[isType + 'total'] += 1
    if state != 13:
        pathways[isType] += 1
        for i in list(set(pathway)):
            pathwayNum[str(i)] += 1

# print(pathways)
# print(pathwayNum)
# sorted_pathways = sorted(pathwayNum.items(), key=operator.itemgetter(1), reverse=True)
# print(sorted_pathways)
print(misfoldedState, "MISFOLDED STATE PERCENTAGES")
for i in ['N', 'M', 'C']:
    print(i, 100 * pathways[i] / pathways[i + 'total'])


misfoldedState = 7
pathways = {'N': 0, 'M': 0, 'C': 0, '?': 0,
            'Ntotal': 0, 'Mtotal': 0, 'Ctotal': 0, '?total': 0}
pathwayNum = {}
for i in range(14):
    pathwayNum[str(i)] = 0
for ifoo in tqdm(range(10000)):
    isType = '?'
    state = 0
    pathway = []
    while state != 13 and state != misfoldedState:
        rand = random.random()
        prevVal = 0
        curVal = 0
        if state == 3 or state == 6:
            isType = 'N'
        if state == 1 or state == 12:
            isType = 'M'
        if state == 11:
            isType = 'C'
        if state == 8:
            isType = '?'
        if state == 0:
            pathway = []
        for i, val in enumerate(m[state, :]):
            curVal += val
            if rand >= prevVal and rand < curVal:
                state = i
                break
            pathway.append(state)
            prevVal = curVal
    pathways[isType + 'total'] += 1
    if state != 13:
        pathways[isType] += 1
        for i in list(set(pathway)):
            pathwayNum[str(i)] += 1

# print(pathways)
# print(pathwayNum)
# sorted_pathways = sorted(pathwayNum.items(), key=operator.itemgetter(1), reverse=True)
# print(sorted_pathways)
print(misfoldedState, "MISFOLDED STATE PERCENTAGES")
for i in ['N', 'M', 'C']:
    print(i, 100 * pathways[i] / pathways[i + 'total'])
