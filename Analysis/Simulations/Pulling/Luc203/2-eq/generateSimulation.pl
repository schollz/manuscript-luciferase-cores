#!/usr/bin/perl

use POSIX;
#if ($#ARGV != 1){
#die "Wrong type of input.\n";
#}

#$file = $ARGV[0];
#$out = $ARGV[1];
#open READ, "<$file" or die $!;
#open OUTFILE, ">$out" or die $!;
my $foundPairs = 0;
#while ($line = <READ>) {
#chomp $line;
my $distance = 542.0*.38; # should be numresidues * 0.345
my $pullSpeed = 2.5; # nm/ns
my $temp =140;# 146;
my $simNum = 1;
my $springConstant = 6; # pN/nm

my $pull_rate1 = $pullSpeed / 1000; # to nm/ps
my $nsteps = floor($distance/$pullSpeed*2000000);
my $i= $temp;
my $kc = $springConstant/1.66; # to kJ / mol / nm^2
`rm -rf pull-$simNum`;

`mkdir pull-$simNum`;
	chdir("pull-$simNum");
	`cp ../eqTemplate.mdp ./pull.mdp`;
	`echo "gen_temp = $i" >> pull.mdp`;
	`echo "gen_vel = yes" >> pull.mdp`;
	`echo "ref_t = $i" >> pull.mdp`;
	`echo "nsteps = $nsteps" >> pull.mdp`;
	`echo "table_extension = 374" >> pull.mdp`;
	`g_grompp -f pull.mdp -n ../../0-build/index.ndx -c ../../0-build/luc203.gro -p ../../0-build/luc203.top -maxwarn 3`;
	system("nohup g_mdrun -noddcheck -table ../../0-build/table.xvg -tablep ../../0-build/table.xvg -nt 2 &");
