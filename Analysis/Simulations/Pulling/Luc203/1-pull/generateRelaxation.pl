#!/usr/bin/perl

use POSIX;

$time=$ARGV[0];
# must manually cp pull.mdp to relax.mdp and change sign of vector
$timeK = $time/1000;
$folderName = "$timeK"."k";
$nsteps =  400000000;
chdir("retract-$folderName");
mkdir("relax");
chdir("relax");
system("cp ../../pull.mdp relax.mdp");
system("sed '/pull/d' relax.mdp > foo"); system("mv foo relax.mdp");
system("sed '/nsteps/d' relax.mdp > foo"); system("mv foo relax.mdp");
system("sed '/gen_vel/d' relax.mdp > foo"); system("mv foo relax.mdp");
`echo "nsteps = $nsteps" >> relax.mdp`;
`echo "gen_vel = yes" >> relax.mdp`;
system("grompp -f relax.mdp -n ../../../../0-build/index.ndx -c ../confout.gro -p ../../../*.top -maxwarn 3");
system("nohup mdrun -noddcheck -table ../../../../0-build/table.xvg -tablep ../../../../0-build/table.xvg -nt 1 &");
