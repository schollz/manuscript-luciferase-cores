#!/usr/bin/perl

use POSIX;

$time=$ARGV[0];
# must manually cp pull.mdp to retract.mdp and change sign of vector
$timeK = $time/1000;
$folderName = "$timeK"."k";
system("mkdir retract-$folderName");
system("echo 0 | trjconv -f traj.trr -dump $time -o retract.g96 -s topol.tpr");
system("vmd -dispdev text -e ../getVector.tcl");
open FILE, "<pull.mdp";
while ($line = <FILE>) {
chomp $line;
@foo = split(" ",$line);
if ($foo[0] eq "dt") {
$dt = $foo[2];
}
}
close FILE;
open FILE, "<pull.mdp";
while ($line = <FILE>) {
chomp $line;
@foo = split(" ",$line);
if ($foo[0] eq "pull_rate1") {
$speed = $foo[2];
}
}
close FILE;


open my $file, '<', "vector.dat"; 
my $firstLine = <$file>; 
close $file;
open my $file, '<', "dist.dat"; 
my $distance = <$file>; 
$distance = $distance/10;
close $file;

$nsteps =  floor ($distance / ($dt * $speed));

system("rm vector.dat");
system("cp pull.mdp retract.mdp");
system("sed '/pull_geometry/d' retract.mdp > foo"); system("mv foo retract.mdp");
system("sed '/pull_dim/d' retract.mdp > foo"); system("mv foo retract.mdp");
system("sed '/pull_vec1/d' retract.mdp > foo"); system("mv foo retract.mdp");
system("sed '/nsteps/d' retract.mdp > foo"); system("mv foo retract.mdp");
system("sed '/pull_start/d' retract.mdp > foo"); system("mv foo retract.mdp");
`echo "pull_vec1 = $firstLine" >> retract.mdp`;
`echo "pull_geometry = position" >> retract.mdp`;
`echo "pull_start = yes" >> retract.mdp`;
`echo "pull_dim = Y Y Y" >> retract.mdp`;
`echo "nsteps = $nsteps" >> retract.mdp`;
#`echo "pull_init1 = $distance" >> retract.mdp`;
system("mv retract.g96 retract-$folderName");
system("mv retract.mdp retract-$folderName");
chdir("retract-$folderName");
system("grompp -f retract.mdp -n ../../../0-build/index.ndx -c retract.g96 -p ../../*.top -maxwarn 3");
system("nohup mdrun -noddcheck -table ../../../0-build/table.xvg -tablep ../../../0-build/table.xvg -nt 1 &");
