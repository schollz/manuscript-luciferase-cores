import sys

start =0
end =437
total = 542

line = ''
pairs = 'pairs' in line
bonds = 'bonds' in line
dihedrals = 'dihedrals' in line
exclusions = 'exclusions' in line
atoms = 'atoms' in line
angles = 'angles' in line

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


with open(sys.argv[1]) as f:
	for line in f:
		parts = line.split()
		skip = False
		if '[' in line:
			pairs = 'pairs' in line
			bonds = 'bonds' in line
			dihedrals = 'dihedrals' in line
			exclusions = 'exclusions' in line
			atoms = 'atoms' in line
			angles = 'angles' in line
		if len(parts)>0 and is_number(parts[0]):
			if pairs or bonds or exclusions:
				for i in range(2):
					if int(parts[i])<start or int(parts[i])>end:
						skip = True
			if dihedrals:
				for i in range(4):
					if int(parts[i])<start or int(parts[i])>end:
						skip = True
			if angles:
				for i in range(3):
					if int(parts[i])<start or int(parts[i])>end:
						skip = True
			if atoms:
				for i in range(1):
					if int(parts[i])<start or int(parts[i])>end:
						skip = True
		if not skip:
			print line,
