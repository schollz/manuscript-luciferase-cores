mol new retract.g96
set smdatom [atomselect top "resid 1"]
set fixedatom [atomselect top "resid 542"]
set smdpos [lindex [$smdatom get {x y z}] 0]	 
set fixedpos [lindex [$fixedatom get {x y z}] 0]	 
set result [vecnorm [vecsub $smdpos $fixedpos]]
set thefile [open "vector.dat" w]
puts $thefile $result
close $thefile

set com1 [measure center $smdatom weight mass]
set com2 [measure center $fixedatom weight mass]
set result  [veclength [vecsub $com1 $com2]]
set thefile [open "dist.dat" w]
puts $thefile $result
close $thefile

exit
