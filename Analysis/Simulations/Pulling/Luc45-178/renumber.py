import sys

def hasNumbers(inputString):
	return any(char.isdigit() for char in inputString)

with open(sys.argv[1],'r') as f:
	for line in f:
		els = line.split()
		for el in els:
			try:
				firstNum = int(el)
				newNum = int(el)-44
				if firstNum>=45 and firstNum<=178:
					if firstNum > 9 and newNum < 10:
						line = line.replace(el,' '+str(newNum)+ 'XX',1)
					elif firstNum > 99 and newNum < 100:
						line = line.replace(el,' '+str(newNum)+ 'XX',1)
					else:
						line = line.replace(el,str(newNum)+'XX',1)
			except:
				pass
		print line.replace('XX',''),
