clear all;
close all;

fileList = getAllFiles('./');
Ls12312937 = [];
Fs12937 = [];
L01110s = [];
L16667s = [];
numPeaks = [];
num = 0
for i=1:length(fileList)
    hasMat = findstr(char(fileList(i)),'s.mat');
    if length(hasMat)>0
       disp(sprintf('%s',char(fileList(i)))) 
       load(char(fileList(i)))
       rec = experiment.recording;
       for j=1:length(rec)
%            plot(rec{j}.x,rec{j}.y)
%            pause(0.2)
            num = num + 1;
            recordings{num} = rec{j};
           if length(rec{j}.L)>1
            L = zeros(length(rec{j}.L),1);
            for k=1:length(L)
                x = rec{j}.xPeaks(k);
                F = rec{j}.yPeaks(k);
                kT=4.1; P=0.45;
                myfun = @(L) F*P/kT-1/(4*(1-x/L).^2)+1/4-x/L;
                L(k)=fzero(myfun,round(x)*1.5);
            end
            try
                Ls12312937 = [Ls12312937; L(2)-L(1)];
                Fs12937= [Fs12937; rec{j}.yPeaks(1)];
                L01110s = [L01110s; L(1)];
            catch
            end
           end
       end
    end
end
size(L01110s)
size(Ls12312937)
size(Fs12937)

data.name = 'Luc46-180';
data.Lc = Ls12312937;
data.F = Fs12937;
data.recordings = recordings;
save(['../LuciferaseTruncations/Analysis/' data.name '.mat'],'data')