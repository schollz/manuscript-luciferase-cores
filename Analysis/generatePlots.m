% save graphic

% print -painters -depsc dist.eps

% Make table
close all;
clear all;
load LuciferaseFull.mat
disp(sprintf('\n\n\n\n\\begin{table}[]'))
disp(sprintf('\\centering'))
disp(sprintf('\\caption{My caption}'))
disp(sprintf('\\label{my-label}'))
disp(sprintf('\\begin{tabular}{| llll|}'))
disp('\hline')
disp('Protein & \begin{tabular}[x]{@{}c@{}}$\Delta$Lc [nm]\\(sim)\end{tabular} & \begin{tabular}[x]{@{}c@{}}$\Delta$Lc [nm]\\(exp)\end{tabular} & $F_u$ [pN] \\')
disp('\hline')
notweird = [];
for i=1:length(allLs12)
   if (allLs12(i,3)>allLs12(i,2) && allLs12(i,1)<60 && allLs12(i,3)>40 && allLs12(i,1)>20 && sum(allLs12(i,:))>190  && sum(allLs12(i,:))<210 )
       notweird = [notweird; allLs12(i,:)];
   end
end
tt = notweird;
notweird = [];
for i=1:length(allLs12)
   if (allLs12(i,3)>allLs12(i,2) && allLs12(i,1)<60 && allLs12(i,3)>40 && allLs12(i,1)>20 && sum(allLs12(i,:))>190  && sum(allLs12(i,:))<210 )
       notweird = [notweird; allFs12(i,:)];
   end
end
tt1=notweird;
simulatedLengths = [(542-446)*.365 68  (280-30)*.365 ];
for i=1:3
disp(sprintf('Luciferase, Peak %d & %2.1f & $ %2.1f \\pm %2.1f $ & $%2.0f \\pm %2.0f $ \\\\',i,simulatedLengths(i),mean(tt(:,i)),std(tt(:,i)),mean(tt1(:,i)),std(tt1(:,i))))
disp('\hline')
end

load Luc1-203.mat
data.Lc = data.Lc(data.F < 100 & data.Lc < 203*.365);
simulatedLength = (182-19)*.365;
disp(sprintf('%s & %2.1f & $ %2.1f \\pm %2.1f $ & $%2.0f \\pm %2.0f $ \\\\',data.name,simulatedLength,mean(data.Lc),std(data.Lc),mean(data.F),std(data.F)))
disp('\hline')

load Luc1-268.mat
data.Lc = data.Lc(data.Lc>60 & data.F < 150 & data.Lc < (268-0)*.365);
simulatedLength = (264-36)*.365;
disp(sprintf('%s & %2.1f & $ %2.1f \\pm %2.1f $ & $%2.0f \\pm %2.0f $ \\\\',data.name,simulatedLength,mean(data.Lc),std(data.Lc),mean(data.F),std(data.F)))
disp('\hline')

load Luc1-450-Peak1.mat
data.Lc = data.Lc(data.Lc>10 & data.Lc < 450*.365);
simulatedLength = (450-265)*.365;
disp(sprintf('%s & %2.1f & $ %2.1f \\pm %2.1f $ & $%2.0f \\pm %2.0f $ \\\\',data.name,simulatedLength,mean(data.Lc),std(data.Lc),mean(data.F),std(data.F)))
disp('\hline')

load Luc1-450-Peak2.mat
data.Lc = data.Lc(data.Lc>10);
simulatedLength = (265-17)*.365;
disp(sprintf('%s & %2.1f & $ %2.1f \\pm %2.1f $ & $%2.0f \\pm %2.0f $ \\\\',data.name,simulatedLength,mean(data.Lc),std(data.Lc),mean(data.F),std(data.F)))
disp('\hline')

load Luc332-440.mat 
data.Lc = data.Lc(data.Lc>10 & data.Lc < (440-332)*.365);
simulatedLength = (432-335)*.365-1.7;
disp(sprintf('%s & %2.1f & $ %2.1f \\pm %2.1f $ & $%2.0f \\pm %2.0f $ \\\\',data.name,simulatedLength,mean(data.Lc),std(data.Lc),mean(data.F),std(data.F)))
disp('\hline')

load Luc46-180.mat
data.Lc = data.Lc(data.Lc>15 & data.Lc < (180-45)*.365);
simulatedLength = 47;
disp(sprintf('%s & %2.1f & $ %2.1f \\pm %2.1f $ & $%2.0f \\pm %2.0f $ \\\\',data.name,simulatedLength,mean(data.Lc),std(data.Lc),mean(data.F),std(data.F)))
disp('\hline')

load Luc433-550.mat
data.Lc = data.Lc(data.Lc>10 & data.Lc < (550-433)*.365);
simulatedLength = (109-2)*.365-2.97;
disp(sprintf('%s & %2.1f & $ %2.1f \\pm %2.1f $ & $%2.0f \\pm %2.0f $ \\\\',data.name,simulatedLength,mean(data.Lc),std(data.Lc),mean(data.F),std(data.F)))
disp('\hline')


disp(sprintf('\\end{tabular}'))
disp(sprintf('\\end{table}'))









close all;
clear all;
iPlot = 0;
figure(1)

iPlot = iPlot + 1;
load LuciferaseFull.mat
notweird = [];
for i=1:length(allLs12)
   if (allLs12(i,3)>allLs12(i,2) && allLs12(i,1)<60 && allLs12(i,3)>40 && allLs12(i,1)>20 && sum(allLs12(i,:))>190  && sum(allLs12(i,:))<210 )
       notweird = [notweird; allLs12(i,:)];
   end
end
tt = notweird;
subplot(8,1,iPlot)
h1 = histogram(tt(:,1),'FaceColor','blue','EdgeColor',[1 1 1]);
fulldata{iPlot}.name = 'Full Peak 1';
fulldata{iPlot}.data = tt(:,1);
hold on
h2 = histogram(tt(:,2),'FaceColor','green','EdgeColor',[1 1 1]);
fulldata{iPlot+1}.name = 'Full Peak 2';
fulldata{iPlot+1}.data = tt(:,2);
h3 = histogram(tt(:,3),'FaceColor','red','EdgeColor',[1 1 1]);
fulldata{iPlot+2}.name = 'Full Peak 3';
fulldata{iPlot+2}.data = tt(:,3);
h3.BinWidth = 5;
h2.BinWidth = 5;
h1.BinWidth = 5;
axis([0 180 0 250])
title('Full Luciferase')
ylabel('Count')

set(gca,'xtick',[])
set(gca,'xticklabel',[])


iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc1-203.mat
data.Lc = data.Lc(data.F < 100 & data.Lc < 203*.365);
h1 = histogram(data.Lc,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.Lc;
h1.BinWidth = 5;
mean(data.Lc)
% line([203*.365 203*.365],[0 41],'LineWidth',4,'Color','red');
line([(182-19)*.365 (182-19)*.365],[0 200],'LineWidth',2,'Color','green')
axis([0 180 0 25])
title(data.name)
set(gca,'xtick',[])
set(gca,'xticklabel',[])

iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc1-268.mat
data.Lc = data.Lc(data.Lc>60 & data.F < 150 & data.Lc < (268-0)*.365);
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.Lc;
h1 = histogram(data.Lc,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
h1.BinWidth = 5;
mean(data.Lc)
% line([268*.365 268*.365],[0 200],'LineWidth',3,'Color','red');
line([(264-36)*.365 (264-36)*.365],[0 41],'LineWidth',2,'Color','green')
axis([0 180 0 30])
title(data.name)
set(gca,'xtick',[])
set(gca,'xticklabel',[])


iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc1-450-Peak1.mat
data.Lc = data.Lc(data.Lc>10);
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.Lc;
h1 = histogram(data.Lc,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
h1.BinWidth = 5;
% line([450*.365 450*.365],[0 41],'LineWidth',3,'Color','red');
line([(450-265)*.365 (450-265)*.365],[0 200],'LineWidth',2,'Color','green')
axis([0 180 0 60])
title(data.name)
set(gca,'xtick',[])
set(gca,'xticklabel',[])


iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc1-450-Peak2.mat
data.Lc = data.Lc(data.Lc>10);
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.Lc;
h1 = histogram(data.Lc,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
h1.BinWidth = 5;
% line([450*.365 450*.365],[0 41],'LineWidth',3,'Color','red');
line([(265-17)*.365 (265-17)*.365],[0 200],'LineWidth',2,'Color','green')
axis([0 180 0 60])
title(data.name)
set(gca,'xtick',[])
set(gca,'xticklabel',[])


iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc46-180.mat
data.Lc = data.Lc(data.Lc>15 & data.Lc < (180-45)*.365);
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.Lc;
h1 = histogram(data.Lc,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
h1.BinWidth = 5;
% line([(180-46)*.365 (180-46)*.365],[0 41],'LineWidth',3,'Color','red');
line([47 47],[0 200],'LineWidth',2,'Color','green')
axis([0 180 0 10])
title(data.name)
set(gca,'xtick',[])
set(gca,'xticklabel',[])

iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc332-440.mat 
data.Lc = data.Lc(data.Lc>15 & data.Lc < (440-332)*.365);
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.Lc;
h1 = histogram(data.Lc,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
h1.BinWidth = 5;
% line([(440-332)*.365 (440-332)*.365],[0 41],'LineWidth',3,'Color','red');
line([(432-335)*.365-1.7 (432-335)*.365-1.7],[0 200],'LineWidth',2,'Color','green')
axis([0 180 0 12])
title(data.name)
set(gca,'xtick',[])
set(gca,'xticklabel',[])


iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc433-550.mat
data.Lc = data.Lc(data.Lc>15 & data.Lc < (550-432)*.365);
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.Lc;
h1 = histogram(data.Lc,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
h1.BinWidth = 5;
% line([(550-433)*.365 (550-433)*.365],[0 41],'LineWidth',3,'Color','red');
line([(109-2)*.365-2.97 (109-2)*.365-2.97],[0 200],'LineWidth',2,'Color','green')
axis([0 180 0 30])
title(data.name)

xlabel('Contour length increment [nm]')

% 
% figure(3)
% disp(sprintf('\n\n'));
% disp('F KS-test, these do not reject null hypothesis that THEY ARE THE SAME')
% N = length(fulldata)*(length(fulldata)-1) / 2;
% allP = zeros(length(fulldata),length(fulldata));
% axisName = {};
% N = N * 2;
% k = 0;
% for i=1:length(fulldata)
%    for j=1:length(fulldata)
%        [h,p,ci] = kstest2(fulldata{i}.data,fulldata{j}.data,'alpha',0.05/N);
%        if (h == 0)
%            disp(sprintf('%s = %s',fulldata{i}.name,fulldata{j}.name))
%        end
%        allP(i,j) = h;
%        allP(j,i) = h;
%    end
%    k = k + 1;
%    axisName{k} = fulldata{i}.name;
% end
% imagesc(1-allP);
% title('Kolmogorov-Smirnov tests for dLc')
% axis square
% Ytick=1:N/2;
% set(gca, ...
%     'Ydir','normal', ...
%     'YTick', Ytick, ...
%     'YTickLabel', axisName, ...
%     'Xdir','normal', ...
%     'XTick', Ytick, ...
%     'XTickLabel', axisName);
% rotateXLabels( gca(), 45 )
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
clear all;
iPlot = 0;
figure(2)


iPlot = iPlot + 1;
load LuciferaseFull.mat
notweird = [];
for i=1:length(allLs12)
   if (allLs12(i,3)>allLs12(i,2) && allLs12(i,1)<60 && allLs12(i,3)>40 && allLs12(i,1)>20 && sum(allLs12(i,:))>190  && sum(allLs12(i,:))<210 )
       notweird = [notweird; allFs12(i,:)];
   end
end
tt=notweird;
subplot(8,1,iPlot)
h1 = histogram(tt(:,1),'FaceColor','blue','EdgeColor',[1 1 1]);
fulldata{iPlot}.name = 'Full Peak 1';
fulldata{iPlot}.data = tt(:,1);
hold on
h2 = histogram(tt(:,2),'FaceColor','green','EdgeColor',[1 1 1]);
fulldata{iPlot+1}.name = 'Full Peak 2';
fulldata{iPlot+1}.data = tt(:,2);
h3 = histogram(tt(:,3),'FaceColor','red','EdgeColor',[1 1 1]);
fulldata{iPlot+2}.name = 'Full Peak 3';
fulldata{iPlot+2}.data = tt(:,3);
h3.BinWidth = 5;
h2.BinWidth = 5;
h1.BinWidth = 5;
axis([0 180 0 180])
title('Full Luciferase')
ylabel('Count')
set(gca,'xtick',[])
set(gca,'xticklabel',[])


iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc1-203.mat
h1 = histogram(data.F,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
h1.BinWidth = 5;
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.F;
axis([0 180 0 25])
title(data.name)
set(gca,'xtick',[])
set(gca,'xticklabel',[])
disp(mean(data.F))
disp(std(data.F))


iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc1-268.mat
data.F = data.F(data.F < 90);
h1 = histogram(data.F,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
h1.BinWidth = 5;
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.F;
axis([0 180 0 30])
title(data.name)
disp(mean(data.F))
disp(std(data.F))
set(gca,'xtick',[])
set(gca,'xticklabel',[])

iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc1-450-Peak1.mat
h1 = histogram(data.F,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
h1.BinWidth = 5;
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.F;
axis([0 180 0 25])
title(data.name)
set(gca,'xtick',[])
set(gca,'xticklabel',[])
disp(mean(data.F))
disp(std(data.F))


iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc1-450-Peak2.mat
h1 = histogram(data.F,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
h1.BinWidth = 5;
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.F;
axis([0 180 0 25])
title(data.name)
set(gca,'xtick',[])
set(gca,'xticklabel',[])
disp(mean(data.F))
disp(std(data.F))


iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc46-180.mat
h1 = histogram(data.F,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
h1.BinWidth = 10;
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.F;
axis([0 180 0 10])
title(data.name)
disp(mean(data.F))
disp(std(data.F))
set(gca,'xtick',[])
set(gca,'xticklabel',[])



iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc332-440.mat
h1 = histogram(data.F,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
h1.BinWidth = 10;
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.F;
axis([0 180 0 8])
title(data.name)
set(gca,'xtick',[])
set(gca,'xticklabel',[])
disp(mean(data.F))
disp(std(data.F))

iPlot = iPlot + 1;
subplot(8,1,iPlot)
load Luc433-550.mat
data.F = data.F(data.F < 40);
h1 = histogram(data.F,'FaceColor',[0.2 0.2 0.2],'EdgeColor',[1 1 1]);
h1.BinWidth = 5;
fulldata{iPlot}.name = data.name;
fulldata{iPlot}.data = data.F;
axis([0 180 0 30])
title(data.name)
disp(mean(data.F))
disp(std(data.F))


xlabel('Unfolding force [pN]')
% 
% figure(4)
% disp(sprintf('\n\n'));
% disp('F KS-test, these do not reject null hypothesis that THEY ARE THE SAME')
% N = length(fulldata)*(length(fulldata)-1) / 2;
% allP = zeros(length(fulldata),length(fulldata));
% axisName = {};
% N = N * 2;
% k = 0;
% for i=1:length(fulldata)
%    for j=1:length(fulldata)
%        [h,p,ci] = kstest2(fulldata{i}.data,fulldata{j}.data,'alpha',0.05/N);
%        if (h == 0)
%            disp(sprintf('%s = %s',fulldata{i}.name,fulldata{j}.name))
%        end
%        allP(i,j) = h;
%        allP(j,i) = h;
%    end
%    k = k + 1;
%    axisName{k} = fulldata{i}.name;
% end
% imagesc(1-allP);
% title('Kolmogorov-Smirnov tests for force')
% axis square
% Ytick=1:N/2;
% set(gca, ...
%     'Ydir','normal', ...
%     'YTick', Ytick, ...
%     'YTickLabel', axisName, ...
%     'Xdir','normal', ...
%     'XTick', Ytick, ...
%     'XTickLabel', axisName);
% rotateXLabels( gca(), 45 )
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% % 
% % 
% % 
% % 
% % figure(5)
% % data = [];
% % data = [data; 203 140 5.5 52.76 28.6];
% % data = [data; 434 140 2 23.2 6.1];
% % data = [data; 437.1 140 16 38.6 16];
% % data = [data; 437.2 140 18 53.7 15];
% % data = [data; 328 140 3 51 20];
% % data = [data; 268 140 17 55.7 17.8];
% % data = [data; 45 140 7 80.9 41];
% % 
% % errorbar(data(:,3),data(:,4),data(:,5),'o')
% % % axis([0 25 0 60])
% % text(data(1,3),data(1,4),'Luc1-203')
% % text(data(3,3),data(3,4),'Luc1-437, Peak 1')
% % text(data(4,3),data(4,4),'Luc1-437, Peak 2')
% % text(data(5,3),data(5,4),'Luc328-437')
% % text(data(6,3),data(6,4),'Luc1-268')
% % text(data(7,3),data(7,4),'Luc45-178')
% % text(data(2,3),data(2,4),'Luc434-550')
% % xlabel('Simulated unfolding force')
% % ylabel('Experimental unfolding force [pN]')
% % title('Comparision between simulated and experimental unfolding forces')
