close all;
figure(1)
subplot(2,1,1)
clusters = importdata('Simulations/clustering2/clusters12/plot0');
colors=colormap(jet(size(clusters,1)));
for i=1:size(clusters,1)
    y = clusters(i,2:end);
   plot(y,'LineWidth',1,'Color',colors(i,:),'LineWidth',2) 
   [yy,xx] = max(y)
   if i==1
   text(xx+10,yy-0.1,'Unfolded','FontSize',14,'Color',colors(i,:))
   elseif i==14
   text(xx-100,yy-0.1,'Folded','FontSize',14,'Color',colors(i,:))       
   end
   hold on;
end
legend('Unfolded','1','2','3','4','5','6','7','8','9','10','11','12','Folded','Location','EastOutside')
title('Kinetics starting from unfolded state')
xlabel('Step')
ylabel('Probability')
axis([0 515 0 1.05])
subplot(2,1,2)
clusters = importdata('Simulations/clustering2/clusters12/plot0');
colors=colormap(jet(size(clusters,1)));
for i=1:size(clusters,1)
   y = clusters(i,2:end);
   plot(y,'LineWidth',1,'Color',colors(i,:),'LineWidth',2) 
   [yy,xx] = max(y)
   text(xx,yy*1.1,sprintf('%d',i-1),'FontSize',14,'Color',colors(i,:))
   hold on;
end
title('Kinetics starting from unfolded state')
% legend('Unfolded','1','2','3','4','5','6','7','8','9','10','11','12','Folded','Location','EastOutside')
xlabel('Step')
ylabel('Probability')
axis([0 515 0 0.12])










close all;
figure(1)
iiii=2;
timeTo50Folded{iiii} = 0;
timeTo50Unfolded{iiii} = 0;
clusters = importdata('Simulations/clustering1/clusters12/plot12');
colors=colormap(jet(size(clusters,1)));
for i=1:size(clusters,1)
    y = clusters(i,2:end);
   plot(y,'LineWidth',1,'Color',colors(i,:),'LineWidth',2) 
   [yy,xx] = max(y)
   if i==13
   text(xx+10,yy-0.1,'12','FontSize',14,'Color',colors(i,:))
   foo = find(y<0.5);
   timeTo50Unfolded{iiii} = foo(1);
   elseif i==14
   text(xx-100,yy-0.1,'Folded','FontSize',14,'Color',colors(i,:))       
   foo = find(y>0.5);
   timeTo50Folded{iiii} = foo(1);
   elseif i==1
   text(xx,yy*1.1,'Unfolded','FontSize',14,'Color',colors(i,:))
   else
   text(xx,yy*1.1,sprintf('%d',i-1),'FontSize',14,'Color',colors(i,:))       
   end
   hold on;
end
legend('Unfolded','1','2','3','4','5','6','7','8','9','10','11','12','Folded','Location','EastOutside')
title('Kinetics starting from state 12')
xlabel('Step')
ylabel('Probability')
axis([0 515 0 1])



close all;
figure(1)
iiii=1;
timeTo50Folded{iiii} = 0;
timeTo50Unfolded{iiii} = 0;
clusters = importdata('Simulations/clustering1/clusters12/plot4');
colors=colormap(jet(size(clusters,1)));
for i=1:size(clusters,1)
    y = clusters(i,2:end);
   plot(y,'LineWidth',1,'Color',colors(i,:),'LineWidth',2) 
   [yy,xx] = max(y)
   if i==5
   text(xx+10,yy-0.1,'4','FontSize',14,'Color',colors(i,:))
   foo = find(y<0.5);
   timeTo50Unfolded{iiii} = foo(1);
   elseif i==14
   text(xx-100,yy-0.1,'Folded','FontSize',14,'Color',colors(i,:))       
   foo = find(y>0.5);
   timeTo50Folded{iiii} = foo(1);
   elseif i==1
   text(xx,yy*1.1,'Unfolded','FontSize',14,'Color',colors(i,:))
   else
   text(xx,yy*1.1,sprintf('%d',i-1),'FontSize',14,'Color',colors(i,:))       
   end
   hold on;
end
legend('Unfolded','1','2','3','4','5','6','7','8','9','10','11','12','Folded','Location','EastOutside')
title('Kinetics starting from unfolded state 4')
xlabel('Step')
ylabel('Probability')
axis([0 515 0 1])


close all;
figure(1)
iiii=3;
timeTo50Folded{iiii} = 0;
timeTo50Unfolded{iiii} = 0;
clusters = importdata('Simulations/clustering1/clusters12/plot2');
colors=colormap(jet(size(clusters,1)));
for i=1:size(clusters,1)
    y = clusters(i,2:end);
   plot(y,'LineWidth',1,'Color',colors(i,:),'LineWidth',2) 
   [yy,xx] = max(y)
   if i==3
   text(xx+10,yy-0.1,'8','FontSize',14,'Color',colors(i,:))
   foo = find(y<0.5);
   timeTo50Unfolded{iiii} = foo(1);
   elseif i==14
   text(xx-100,yy-0.1,'Folded','FontSize',14,'Color',colors(i,:))       
   foo = find(y>0.5);
   timeTo50Folded{iiii} = foo(1);
   elseif i==1
   text(xx,yy*1.1,'Unfolded','FontSize',14,'Color',colors(i,:))
   else
   text(xx,yy*1.1,sprintf('%d',i-1),'FontSize',14,'Color',colors(i,:))       
   end
   hold on;
end
legend('Unfolded','1','2','3','4','5','6','7','8','9','10','11','12','Folded','Location','EastOutside')
title('Kinetics starting from state 2')
xlabel('Step')
ylabel('Probability')
axis([0 515 0 1])

