close all;
contacts = importdata('Simulations/contacts.txt');
clusters = importdata('Simulations/clustering2/clusters12/clusters.out');

size(contacts)
size(clusters)
ha = tight_subplot(3,4,[.01 .03],[.1 .01],[.01 .01])
%           for ii = 1:6; plot(randn(10,ii)); end
%           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')

for j=1:size(clusters,1)
axes(ha(j));
colormap(flipud(bone))
% subplot(2,1,1)
% plot(clusters(j,:))
% subplot(2,1,2)
plot(clusters(4,:))
contactMap = zeros(max(contacts(:,1))+2,max(contacts(:,1))+2);
for i=1:size(contacts,1)
   contactMap(contacts(i,1)+1,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)) = clusters(j,i);
   
   contactMap(contacts(i,2)+1,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)) = clusters(j,i);
end
imagesc(contactMap)
text(400,100,sprintf('%d',j),'FontSize',20)
axis square
axis tight
end
set(ha(1:12),'XTickLabel','');
set(ha(1:12),'YTickLabel','');







close all;
contacts = importdata('Simulations/contacts.txt');
clusters = importdata('Simulations/clustering2/clusters18/clusters.out');

size(contacts)
size(clusters)
ha = tight_subplot(3,6,[.01 .03],[.1 .01],[.01 .01])
%           for ii = 1:6; plot(randn(10,ii)); end
%           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')

for j=1:size(clusters,1)
axes(ha(j));
colormap(flipud(bone))
% subplot(2,1,1)
% plot(clusters(j,:))
% subplot(2,1,2)
plot(clusters(4,:))
contactMap = zeros(max(contacts(:,1))+2,max(contacts(:,1))+2);
for i=1:size(contacts,1)
   contactMap(contacts(i,1)+1,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)) = clusters(j,i);
   
   contactMap(contacts(i,2)+1,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)) = clusters(j,i);
end
imagesc(contactMap)
text(400,100,sprintf('%d',j),'FontSize',20)
axis square
axis tight
end
set(ha(1:18),'XTickLabel','');
set(ha(1:18),'YTickLabel','');







close all;
contacts = importdata('Simulations/contacts.txt');
clusters = importdata('Simulations/clustering1/clusters36/clusters.out');

size(contacts)
size(clusters)
ha = tight_subplot(6,6,[.01 .03],[.1 .01],[.01 .01])
%           for ii = 1:6; plot(randn(10,ii)); end
%           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')

for j=1:size(clusters,1)
axes(ha(j));
colormap(flipud(bone))
% subplot(2,1,1)
% plot(clusters(j,:))
% subplot(2,1,2)
plot(clusters(4,:))
contactMap = zeros(max(contacts(:,1))+2,max(contacts(:,1))+2);
for i=1:size(contacts,1)
   contactMap(contacts(i,1)+1,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)) = clusters(j,i);
   
   contactMap(contacts(i,2)+1,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)) = clusters(j,i);
end
imagesc(contactMap)
text(400,100,sprintf('%d',j),'FontSize',20)
axis square
axis tight
end
set(ha(1:36),'XTickLabel','');
set(ha(1:36),'YTickLabel','');









figure(1)
contacts = importdata('Simulations/contacts.txt');
clusters = importdata('Simulations/clustering2/clusters12/clusters.out');

size(contacts)
size(clusters)
ha = tight_subplot(1,3,[.01 .03],[.1 .01],[.01 .01])
%           for ii = 1:6; plot(randn(10,ii)); end
%           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')

js = [4,12,2];
percents = [0.81,0.31,0.14];
for jj=1:length(js)
    j=js(jj);
axes(ha(jj));
colormap(flipud(bone))
% subplot(2,1,1)
% plot(clusters(j,:))
% subplot(2,1,2)
contactMap = zeros(max(contacts(:,1))+2,max(contacts(:,1))+2);
for i=1:size(contacts,1)
   contactMap(contacts(i,1)+1,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)) = clusters(j,i);
   
   contactMap(contacts(i,2)+1,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)) = clusters(j,i);
end
imagesc(contactMap)
text(400,100,sprintf('%d',j),'FontSize',20)
text(40,470,sprintf('%2.2f%%',percents(jj)),'FontSize',18)
if jj==2
    title('First states from unfolded, 14 clusters')
end
axis square
axis tight
end
set(ha(1:3),'XTickLabel','');
set(ha(1:3),'YTickLabel','');



figure(2)
contacts = importdata('Simulations/contacts.txt');
clusters = importdata('Simulations/clustering1/clusters18/clusters.out');

size(contacts)
size(clusters)
ha = tight_subplot(1,3,[.01 .03],[.1 .01],[.01 .01])
%           for ii = 1:6; plot(randn(10,ii)); end
%           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')

js = [5,8,17];
percents = [0.79,0.31,0.15];
for jj=1:length(js)
    j=js(jj);
axes(ha(jj));
colormap(flipud(bone))
% subplot(2,1,1)
% plot(clusters(j,:))
% subplot(2,1,2)
contactMap = zeros(max(contacts(:,1))+2,max(contacts(:,1))+2);
for i=1:size(contacts,1)
   contactMap(contacts(i,1)+1,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)) = clusters(j,i);
   
   contactMap(contacts(i,2)+1,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)) = clusters(j,i);
end
imagesc(contactMap)
text(400,100,sprintf('%d',j),'FontSize',20)
text(40,470,sprintf('%2.2f%%',percents(jj)),'FontSize',18)
if jj==2
    title('First states from unfolded, 20 clusters')
end
axis square
axis tight
end
set(ha(1:3),'XTickLabel','');
set(ha(1:3),'YTickLabel','');






figure(3)
contacts = importdata('Simulations/contacts.txt');
clusters = importdata('Simulations/clustering1/clusters36/clusters.out');

size(contacts)
size(clusters)
ha = tight_subplot(1,3,[.01 .03],[.1 .01],[.01 .01])
%           for ii = 1:6; plot(randn(10,ii)); end
%           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')

js = [17,11,15];
percents = [0.79,0.31,0.15];
for jj=1:length(js)
    j=js(jj);
axes(ha(jj));
colormap(flipud(bone))
% subplot(2,1,1)
% plot(clusters(j,:))
% subplot(2,1,2)
contactMap = zeros(max(contacts(:,1))+2,max(contacts(:,1))+2);
for i=1:size(contacts,1)
   contactMap(contacts(i,1)+1,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)) = clusters(j,i);
   
   contactMap(contacts(i,2)+2,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)+1) = clusters(j,i);

end
imagesc(contactMap)
text(400,100,sprintf('%d',j),'FontSize',20)
text(40,470,sprintf('%2.2f%%',percents(jj)),'FontSize',18)
if jj==2
    title('First states from unfolded, 38 clusters')
end
axis square
axis tight
end
set(ha(1:3),'XTickLabel','');
set(ha(1:3),'YTickLabel','');


axis square
axis tight
xlabel('Residue')
ylabel('Residue')
axis([1 471 1 471])












close all;
contacts = importdata('Simulations/contacts.txt');
clusters = importdata('Simulations/clustering1/clusters12/clusters.out');

size(contacts)
size(clusters)
%           for ii = 1:6; plot(randn(10,ii)); end
%           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')

j=6;
colormap(flipud(bone))
% subplot(2,1,1)
% plot(clusters(j,:))
% subplot(2,1,2)
plot(clusters(4,:))
contactMap = zeros(max(contacts(:,1))+2,max(contacts(:,1))+2);
for i=1:size(contacts,1)
   contactMap(contacts(i,1)+1,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)) = clusters(j,i);
   
   contactMap(contacts(i,2)+1,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)) = clusters(j,i);
end
imagesc(contactMap)
text(400,100,sprintf('%d',j),'FontSize',20)
axis square
axis tight
xlabel('Residue')
ylabel('Residue')
axis([200 350 200 350])


close all;
contacts = importdata('Simulations/contacts.txt');
clusters = importdata('Simulations/clustering1/clusters12/clusters.out');

size(contacts)
size(clusters)
%           for ii = 1:6; plot(randn(10,ii)); end
%           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')

j=8;
colormap(flipud(bone))
% subplot(2,1,1)
% plot(clusters(j,:))
% subplot(2,1,2)
plot(clusters(4,:))
contactMap = zeros(max(contacts(:,1))+2,max(contacts(:,1))+2);
for i=1:size(contacts,1)
   contactMap(contacts(i,1)+1,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)) = clusters(j,i);
   
   contactMap(contacts(i,2)+1,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)) = clusters(j,i);
end
imagesc(contactMap)
text(400,100,sprintf('%d',j),'FontSize',20)
axis square
axis tight
xlabel('Residue')
ylabel('Residue')
axis([70 250 70 250])



close all;
contacts = importdata('Simulations/contacts.txt');
clusters = importdata('Simulations/clustering1/clusters18/clusters.out');

size(contacts)
size(clusters)
%           for ii = 1:6; plot(randn(10,ii)); end
%           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')

j=1;
colormap(flipud(bone))
% subplot(2,1,1)
% plot(clusters(j,:))
% subplot(2,1,2)
plot(clusters(4,:))
contactMap = zeros(max(contacts(:,1))+2,max(contacts(:,1))+2);
for i=1:size(contacts,1)
   contactMap(contacts(i,1)+1,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1)+2,contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)+2) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)+1) = clusters(j,i);
   contactMap(contacts(i,1)+1,contacts(i,2)) = clusters(j,i);
   contactMap(contacts(i,1),contacts(i,2)) = clusters(j,i);
   
   contactMap(contacts(i,2)+1,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2)+2,contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)+2) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)+1) = clusters(j,i);
   contactMap(contacts(i,2)+1,contacts(i,1)) = clusters(j,i);
   contactMap(contacts(i,2),contacts(i,1)) = clusters(j,i);
end
imagesc(contactMap)
text(400,100,sprintf('%d',j),'FontSize',20)
axis square
axis tight
xlabel('Residue')
ylabel('Residue')
axis([20 180 20 180])
