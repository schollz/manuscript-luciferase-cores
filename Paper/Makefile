filename=LuciferaseDissection_v7

pdflatex: clean
	pdflatex ${filename}.tex
	pdflatex ${filename}.tex
	bibtex ${filename}
	pdflatex ${filename}.tex
	pdflatex ${filename}.tex
	echo "Generating supplement"
	pdftk ${filename}.pdf cat 30-45 output supplement.pdf
	pdftk ${filename}.pdf cat 1-29 output final.pdf
	zip FinalVersion.zip FinalReferences.bib final.pdf LuciferaseDissection_v7.tex
	# xpdf ${filename}.pdf

pdf: ps
	ps2pdf ${filename}.ps

zip: zip
	rm -rf submission.zip
	zip -9 submission.zip ${filename}.tex Figures/*png Papers/References3.bib

pdf-print: ps
	ps2pdf -dColorConversionStrategy=/LeaveColorUnchanged -dPDFSETTINGS=/printer ${filename}.ps

text: html
	html2text -width 100 -style pretty ${filename}/${filename}.html | sed -n '/./,$$p' | head -n-2 >${filename}.txt

html:
	@#latex2html -split +0 -info "" -no_navigation ${filename}
	htlatex ${filename}

ps:	dvi
	dvips -t letter ${filename}.dvi

dvi:
	latex ${filename}
	bibtex ${filename}||true
	latex ${filename}
	latex ${filename}

read:
	evince ${filename}.pdf &

aread:
	acroread ${filename}.pdf

clean:
	rm -f ${filename}.pdf
	rm -f ${filename}.log
	rm -f ${filename}.aux
	rm -f ${filename}.out
	rm -f ${filename}.dvi
	rm -f ${filename}.bbl
	rm -f acs-${filename}.bbl
	rm -f ${filename}.blg

word:
	pandoc -o ${filename}.docx -s ${filename}.tex --bibliography=./Papers/References2.bib --csl biophysical-journal.csl

diff:
	latexdiff LuciferaseDissection_v5.tex ${filename}.tex > diff.tex
	pdflatex diff.tex
	pdflatex diff.tex
	bibtex diff
	pdflatex diff.tex
	pdflatex diff.tex
